﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageScript : MonoBehaviour 
{
	[HideInInspector]
	public float damage;

	void OnTriggerEnter2D(Collider2D target)
	{
		Debug.Log("test");

		if(target.GetComponent<Health>() == null)
		{
			return;
		}

		if(target.isTrigger != true)
		{
			target.GetComponent<Health>().health -= damage;
		}
	}
}