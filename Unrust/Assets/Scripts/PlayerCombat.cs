﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour 
{
	public float damage;
	public float attackCd;
	public Collider2D attackCollider;
	public Transform attackPoint;

	private bool attacking;
	private float attackTimer;

	void Awake()
	{
		attacking = false;
		attackCollider.GetComponent<DamageScript>().damage = damage;
		attackCollider.enabled = false;
	}

	void Update()
	{
		if(Input.GetKey(KeyCode.G) || Input.GetButton("AButton") && !attacking)
		{
			attacking = true;
			attackTimer = attackCd;
			//attackCollider.enabled = true;
			Attack();
		}
		if(attacking)
		{
			if(attackTimer> 0)
			{
				attackTimer -= Time.deltaTime;
			}
			else
			{
				attacking = false;
				attackCollider.enabled = false;
			}
		}
	}

	void Attack()
	{
		Collider2D[] obejectsHit = Physics2D.OverlapBoxAll(attackPoint.transform.position, new Vector2(0.3f,0.2f), 0);
		if(obejectsHit.Length >= 1)
		{
			foreach (Collider2D item in obejectsHit)
			{
				item.GetComponent<Health>().health -= damage;
			}
		}
	}
}