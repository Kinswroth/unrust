﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public int plyrSpd;
    void Update ()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetAxis("LeftJoyStickVertical") > 0 || Input.GetAxis("DirPadVertical") > 0)
        {
            transform.position += new Vector3(0, 1 * plyrSpd * Time.deltaTime, 0);
            transform.rotation = Quaternion.Euler(0,0,0);
        }
        if (Input.GetKey(KeyCode.A) || Input.GetAxis("LeftJoyStickHorizontal") < 0 || Input.GetAxis("DirPadHorizontal") < 0)
        {
            transform.position += new Vector3(-1 * plyrSpd * Time.deltaTime, 0, 0);
            transform.rotation = Quaternion.Euler(0,0,90);
        }
        if (Input.GetKey(KeyCode.S) || Input.GetAxis("LeftJoyStickVertical") < 0 || Input.GetAxis("DirPadVertical") < 0)
        {
            transform.position += new Vector3(0, -1 * plyrSpd * Time.deltaTime, 0);
            transform.rotation = Quaternion.Euler(0,0,180);
        }
        if (Input.GetKey(KeyCode.D) || Input.GetAxis("LeftJoyStickHorizontal") > 0 || Input.GetAxis("DirPadHorizontal") > 0)
        {
            transform.position += new Vector3(1 * plyrSpd * Time.deltaTime, 0, 0);
            transform.rotation = Quaternion.Euler(0,0,270);
        }
    }
}